<?php
/**
 * Plugin Name: Test Plugin No Tags
 * Plugin URI: https://bitbucket.org/afragen/test-plugin/
 * Bitbucket Plugin URI: afragen/test-plugin-notags/
 * Description: This plugin is used for testing functionality of Bitbucket updating of plugins.
 * Version: 0.1.3
 * Author: Andy Fragen
 * License: GNU General Public License v2
 * License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
